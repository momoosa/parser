//
//  TagSuggestionCollectionViewCell.swift
//  Parser
//
//  Created by Mo Moosa on 08/09/2016.
//  Copyright © 2016 Mo Moosa. All rights reserved.
//

import UIKit

class TagSuggestionCollectionViewCell: UICollectionViewCell {
	let titleLabel = UILabel()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		self.contentView.addSubview(self.titleLabel)
		self.titleLabel.translatesAutoresizingMaskIntoConstraints = false

		self.leadingAnchor.constraint(equalTo: self.titleLabel.leadingAnchor).isActive = true
		self.topAnchor.constraint(equalTo: self.titleLabel.topAnchor).isActive = true
		self.trailingAnchor.constraint(equalTo: self.titleLabel.trailingAnchor).isActive = true
		self.bottomAnchor.constraint(equalTo: self.titleLabel.bottomAnchor).isActive = true
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	
	override func prepareForReuse() {
		
		self.titleLabel.text = nil
	}
}

