//
//  TagsSuggestionView.swift
//  Parser
//
//  Created by Mo Moosa on 08/09/2016.
//  Copyright © 2016 Mo Moosa. All rights reserved.
//

import UIKit

let numberOfSections = 1
let TagSuggestionCellIdentifier = "TagSuggestionCell"

protocol TagSuggestionsViewDelegate: class  {
	
	func tagSuggestionsView(tagSuggestionsView: TagsSuggestionView, didSelectTag tag: String)
}

class TagsSuggestionView: UIView {
	
	weak var delegate: TagSuggestionsViewDelegate?
	private var collectionView: UICollectionView?
	var currentTags = [String?]()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.backgroundColor = Theme.Color.lightGray
		
		let layout = UICollectionViewFlowLayout()
		self.collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
		
		if let collectionView = self.collectionView {
			
			collectionView.translatesAutoresizingMaskIntoConstraints = false
			self.addSubview(collectionView)
			collectionView.backgroundColor = nil
			
			let padding: CGFloat = 8.0
			
			collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: padding).isActive = true
			self.topAnchor.constraint(equalTo: collectionView.topAnchor).isActive = true
			collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: padding).isActive = true
			self.bottomAnchor.constraint(equalTo: collectionView.bottomAnchor).isActive = true
			
			collectionView.register(TagSuggestionCollectionViewCell.self, forCellWithReuseIdentifier: TagSuggestionCellIdentifier)
			
			collectionView.delegate = self
			collectionView.dataSource = self
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func update(newTags: [String?]?) {
		
		if let newTagsToUpdate = newTags {
			
			self.currentTags = newTagsToUpdate
			
		}
		else {
			
			self.currentTags.removeAll()
		}
		
		self.collectionView?.performBatchUpdates({
			self.collectionView?.reloadSections(IndexSet(integer: 0))
			
			}, completion: nil)
	}
}


extension TagsSuggestionView: UICollectionViewDataSource {
	
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		
		return 1
	}
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		
		return self.currentTags.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let tag = self.currentTags[indexPath.row]
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TagSuggestionCellIdentifier, for: indexPath) as! TagSuggestionCollectionViewCell
		
		cell.titleLabel.text = tag
		
		return cell
	}
}

extension TagsSuggestionView: UICollectionViewDelegate {
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		
		if let tag = self.currentTags[indexPath.row] {
			
			self.delegate?.tagSuggestionsView(tagSuggestionsView: self, didSelectTag: tag)
		}
	}
}
