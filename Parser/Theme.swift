//
//  Theme.swift
//  Parser
//
//  Created by Mo Moosa on 08/09/2016.
//  Copyright © 2016 Mo Moosa. All rights reserved.
//

import UIKit

struct Theme {

	struct Color {
		
		static let darkBlue = UIColor(red:0.14, green:0.22, blue:0.45, alpha:1.00)
		static let mediumBlue = UIColor(red:0.14, green:0.32, blue:0.55, alpha:1.00)
		static let lightBlue = UIColor(red:0.39, green:0.59, blue:0.84, alpha:1.00)
		static let lightGray = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.00)
	}
}
