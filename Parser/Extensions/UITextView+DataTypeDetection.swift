//
//  UITextView+DataTypeDetection.swift
//  Parser
//
//  Created by Mo Moosa on 07/09/2016.
//  Copyright © 2016 Mo Moosa. All rights reserved.
//

import UIKit

extension UITextView {

	func isEditingHashTag() -> Bool {
		
		if self.currentEditedHashTag() != nil {
			
			return true
		}
		return false
	}
	
	func currentEditedHashTag() -> String?{
		
		if let textRange = self.selectedTextRange {
		
			if let text = self.text {
				
				let cursorPosition = self.offset(from: self.beginningOfDocument, to: textRange.start)
				
				let index = text.index(text.startIndex, offsetBy: cursorPosition)
				
				let substring = text.substring(to: index)
				
				if let lastWord = substring.components(separatedBy: " ").last {
					
					if lastWord.characters.first == "#" {
						
						return lastWord
					}
				}
			}
		}
		
		return nil

	}
}
