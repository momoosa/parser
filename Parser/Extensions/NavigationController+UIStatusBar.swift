//
//  NavigationController+UIStatusBar.swift
//  Parser
//
//  Created by Mo Moosa on 08/09/2016.
//  Copyright © 2016 Mo Moosa. All rights reserved.
//

import UIKit

extension UINavigationController {
	
	open override var preferredStatusBarStyle: UIStatusBarStyle {
		
		return .lightContent
	}
}
