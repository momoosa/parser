//
//  Tag.swift
//  Parser
//
//  Created by Mo Moosa on 08/09/2016.
//  Copyright © 2016 Mo Moosa. All rights reserved.
//

import Foundation

class Tag {
	
	/** The UI-facing representation of the tag, with a # prefix. For example,
		if `tag` is "What" `displayText` will be "#What".
	*/
	
	var displayText: String? {
		
		get {
			
			guard let text = self.text, text.characters.count > 0 else {
				
				// No tag, no point in showing anything.
				
				return nil
			}
			
			// If `tag` is "What" then "#What" will be returned.
			
			return "#\(self.text)"
		}
	}
	
	/** The underlying content of the tag without any extra identifiers (such as the actual # itself). */
	
	var text: String?
	
	/** The position of the tag in its text container. This helps distinguish between two tags with the same text but
	 in different parts of the text container. */
	
	var textPosition: TextPosition?
}

extension Tag: Hashable {
	
	var hashValue: Int {
		
		guard let text = self.text else {
			
			return 0
		}
		
		return text.hash
	}
}

func ==(lhs: Tag, rhs: Tag) -> Bool {
	
	return lhs.hashValue == rhs.hashValue && lhs.textPosition == rhs.textPosition
}
