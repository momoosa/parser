//
//  DataTypeParser.swift
//  Parser
//
//  Created by Mo Moosa on 07/09/2016.
//  Copyright © 2016 Mo Moosa. All rights reserved.
//

import UIKit

enum DataParserType: Int {
	case hashTag, url
	
	/** Returns the appropriate regex pattern for use with `NSRegularExpression`. */
	
	func regexPattern() -> String {
		
		switch self {
			
		case .hashTag:
			
			return "[#]{1}\\w+"
			
		case .url:
			
			return "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
		}
	}
}

enum DataParserErrorType: Error {
	case regularExpressionSyntax, unknown
}

public class DataTypeParser {
	
	var highlightColor = Theme.Color.mediumBlue
	var defaultColor = UIColor.black
	var existingTags = [TextPosition]()
	
	/** UITextView convenience method for `attributedStringWithInternetVariables(string, defaultColor, highlightColor)`.	*/
	
	public func formatTextViewForInternetVariables(_ textView: UITextView) {
		
		guard let count = textView.text?.characters.count, count > 0 else {
			
			return
		}
		
		textView.attributedText = self.attributedStringWithInternetVariables(textView.text, defaultColor: defaultColor, highlightColor: highlightColor)
	}
	
	/** Formats the string with the preferred `highlightColor` and `defaultColor` based
	on the hashtags and URLs.
	*/

	public func attributedStringWithInternetVariables(_ string: String, defaultColor: UIColor?, highlightColor: UIColor?) -> NSAttributedString? {
		
		guard string.characters.count > 0 else {
			
			return nil
		}
		
		let defaultTextColor : UIColor = (defaultColor != nil ? defaultColor! : UIColor.black)
		
		let highlightTextColor : UIColor = (highlightColor != nil ? highlightColor! : UIColor.blue)
		
		let hashTagVariables = [NSForegroundColorAttributeName: highlightTextColor, NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
		
		let attributedString = NSMutableAttributedString(string: string, attributes: [NSForegroundColorAttributeName: defaultTextColor, NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)])
		
		let range = NSMakeRange(0, attributedString.string.characters.count)
		
		do {
			
			let hashRegularExpression = try NSRegularExpression(pattern: DataParserType.hashTag.regexPattern(), options: NSRegularExpression.Options.caseInsensitive)
			let urlRegularExpression = try NSRegularExpression(pattern: DataParserType.url.regexPattern(), options: NSRegularExpression.Options.caseInsensitive)
			
			let hashMatches = hashRegularExpression.matches(in: attributedString.string, options: NSRegularExpression.MatchingOptions.reportCompletion, range: range)
			let urlMatches = urlRegularExpression.matches(in: attributedString.string, options: NSRegularExpression.MatchingOptions.reportCompletion, range: range)
			
			for match in hashMatches {
				
				let wordRange = match.rangeAt(0)
				attributedString.setAttributes(hashTagVariables, range: wordRange)
			}
			
			for match in urlMatches {
				
				let wordRange = match.rangeAt(0)
				attributedString.setAttributes(hashTagVariables, range: wordRange)
			}
						
			return attributedString
		}
		catch {
			
			let nserror = error as NSError
			
			NSLog("Error attempting to parse string: \(nserror) with regular expression, \(nserror.userInfo)")
			
			return nil
		}
	}
	
	
	/** Extracts URLS and hashtags from the given string.
	- Parameter text: The text to be parsed.
	- Returns: Returns a tuple containing one array of `TextPosition` instances representing tags and another 
	array of `TextPosition` instances representing URLs.
	*/

	public class func extractInternetVariables(_ text: String?) -> (tags: [TextPosition], urls: [TextPosition]) {
		
		var tagTextPositions = [TextPosition]()
		var urlTextPositions = [TextPosition]()
		
		guard (text?.characters.count)! > 0 else {
			
			return (urlTextPositions, tagTextPositions)
		}
		
		let sourceText: NSString = text! as NSString
		
		do {
			
			let hashRegularExpression = try NSRegularExpression(pattern: DataParserType.hashTag.regexPattern(), options: NSRegularExpression.Options.caseInsensitive)
			let urlRegularExpression = try NSRegularExpression(pattern: DataParserType.url.regexPattern(), options: NSRegularExpression.Options.caseInsensitive)
			
			let matches = hashRegularExpression.matches(in: sourceText as String, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, sourceText.length))
			let urlMatches = urlRegularExpression.matches(in: sourceText as String, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSMakeRange(0, sourceText.length))
			
			let matchClosure: (NSTextCheckingResult) -> (TextPosition) = { match in
				
				let wordRange = match.rangeAt(0)
				
				var matchingString = sourceText.substring(with: wordRange)
				
				matchingString.remove(at: matchingString.startIndex)
				
				return TextPosition(text: matchingString, range: wordRange)
			}
			
			let newTags = matches.map(matchClosure)
			
			tagTextPositions.append(contentsOf: newTags)
						
			let newURLs = urlMatches.map(matchClosure)
			
			urlTextPositions.append(contentsOf: newURLs)
		}
		catch {

			
			print("Error attempting to parse string: \(error)")
		}
		
		return (tagTextPositions, urlTextPositions)
	}
	
	/** Convenience method for creating `Tag` instances out of `TextPosition` instances.
	- Parameter textPositions: The array of textPositions to be converted.
	- Returns: Returns an array of `Tag` objects.
	*/
	func createTags(forTextPositions textPositions: [TextPosition]) -> [Tag] {
		
		let textPositionClosure: (TextPosition) -> (Tag) = { textPosition in
			
			let tag = Tag()
			
			tag.textPosition = textPosition
			tag.text = textPosition.text
			
			return tag
		}
		
		let tags = textPositions.map(textPositionClosure)
		
		return tags
	}
}
