//
//  TagsController.swift
//  Parser
//
//  Created by Mo Moosa on 07/09/2016.
//  Copyright © 2016 Mo Moosa. All rights reserved.
//

/** One day this controller might persist the tags or send them to a server, but for now it
 simply stores them in the current session.
**/

import Foundation

class TagsController {
	
	private var existingTags = [Tag]()
	var displayableTags: Set <Tag> {
		
		get {
			
			let set = Set(self.existingTags)
			
			return set
		}
	}
	
	/** Updates the `TagsController`'s current set of tags. If any of the new tags 
	overlap existing tags then the existing tags are replaced.
	
	- Parameter tags: The new tags to add.
	*/
	
	func update(newTags tags: [Tag]) {
		
		for newTag in tags {
			
			for existingTag in self.existingTags {
				
				if let newTextPosition = newTag.textPosition, let existingTextPosition = existingTag.textPosition {
					
					if newTextPosition.overlapsTextPosition(existingTextPosition) {
						
						self.existingTags.remove(at: (self.existingTags.index(of: existingTag))!)
					}
				}
			}

		self.existingTags.append(contentsOf: tags)
			
		}
	}

	/** Check for strings that are similar to the given string.
	overlap existing tags then the existing tags are replaced.
	
	- Parameter stringToCheck: The string to check against.
	*/
	
	func checkForSimilarTags(to stringToCheck: String) -> [String?]{
		let displayableTags = self.displayableTags
		
		var stringToCheck = stringToCheck
		
		if stringToCheck.characters.first == "#" {
			
			// So we're now comparing "ABC" and "ABC" instead of "#ABC" and "ABC".
			stringToCheck.remove(at: stringToCheck.startIndex)
		}
		
		var similarTags = [String]()
		
		for existingTag in displayableTags {
			
			if let text = existingTag.text, text.characters.count > 0 {
				
				// Shouldn't return the same tag we're checking against.
				
				if text.contains(stringToCheck) && text != stringToCheck{
					
					similarTags.append(text)
				}
			}
		}
		
		return similarTags
	}

}
