//
//  TextPosition.swift
//  Parser
//
//  Created by Mo Moosa on 07/09/2016.
//  Copyright © 2016 Mo Moosa. All rights reserved.
//

import Foundation
/**
TextPosition is a class that represents the layout position of a string in another string.
The `TextPosition` stores the text it represents as well as the range of the text in its
enclosing string.
*/

public struct TextPosition {
	
	/** The text that the `TextPosition` represents. */
	
	public var text: String?
	
	/** The range of the `text` in its enclosing string. For example if the `TextPosition`
	* represents the word "fox" in "The fox and the hound" then `range` would be `NSRange(4, 3)`.
	*/
	
	public var range: NSRange?
	
	init(text: String, range: NSRange) {
		
		self.text = text
		self.range = range
	}
	
	/** Used to determine whether or not one `TextPosition` overlaps another `TextPosition`. This can be handy
	for checking if the user editing text at the position of an existing `TextPosition`.
	
	- Parameter otherTextPosition: The other `TextPosition` to check for overlapping with.
	- Returns: Returns `true` if the range of either `TextPosition` starts/ends in the range of
	the other `TextPosition`. Returns `false` if this condition is not met or if one `TextPosition` has no range.
	*/
	
	public func overlapsTextPosition(_ otherTextPosition: TextPosition) -> Bool {
		
		guard let range = self.range, let otherRange = otherTextPosition.range else {
			
			return false
		}
		
		if range.location == otherRange.location &&
			(range.length >= otherRange.length || range.length <= otherRange.length) {
			
			return true
		}
		
		return false
	}
}

extension TextPosition: Hashable {
	
	public var hashValue: Int {
		
		// Used for comparing two text positions for equality.
		
		var defaultHash = 0
		
		if let text = self.text {
			
			defaultHash += text.hashValue
		}
		
		if let range = self.range {
			
			defaultHash += range.location + range.length
		}
		
		return defaultHash
	}
}

public func ==(lhs: TextPosition, rhs: TextPosition) -> Bool {
	
	return lhs.hashValue == rhs.hashValue
}
