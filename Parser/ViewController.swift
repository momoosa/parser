//
//  ViewController.swift
//  Parser
//
//  Created by Mo Moosa on 07/09/2016.
//  Copyright © 2016 Mo Moosa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	let infoLabel = UILabel()
	let textView = UITextView()
	private var bottomConstraint: NSLayoutConstraint?
	let dataTypeParser = DataTypeParser()
	private let tagsSuggestionView = TagsSuggestionView()
	let tagsController = TagsController()
	var shouldShowKeyboardOnViewWillAppear = true
	
	// MARK: View Lifecycle
	
	override func viewDidLoad() {
		
		super.viewDidLoad()

		// General view setup
		
		self.navigationController?.navigationBar.barTintColor = Theme.Color.darkBlue
		self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
		self.navigationController?.navigationBar.isTranslucent = false
		
		self.title = NSLocalizedString("Parser", comment: "The title of the app")
		self.edgesForExtendedLayout = []
		self.view.backgroundColor = Theme.Color.lightGray
		
		self.view.addSubview(self.infoLabel)
		
		// Info label - explains to the user how to use the demo.
		
		self.infoLabel.translatesAutoresizingMaskIntoConstraints = false
		
		self.infoLabel.text = NSLocalizedString("Type whatever you like textview below. Hashtags & URLs will be highlighted.",
		                                        comment: "Info text to explain how the user should use the demo.")
		self.infoLabel.numberOfLines = 2
		self.infoLabel.textAlignment = .center
		
		let padding: CGFloat = 8.0
		
		self.infoLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: padding).isActive = true
		self.infoLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: padding).isActive = true
		self.view.trailingAnchor.constraint(equalTo: self.infoLabel.trailingAnchor, constant: padding).isActive = true
		
		// Textview setup
		self.view.addSubview(self.textView)
		self.self.textView.topAnchor.constraint(equalTo: self.infoLabel.bottomAnchor, constant: padding).isActive = true
		
		self.textView.translatesAutoresizingMaskIntoConstraints = false
		self.textView.delegate = self
		self.view.leadingAnchor.constraint(equalTo: self.textView.leadingAnchor).isActive = true
		self.view.trailingAnchor.constraint(equalTo: self.textView.trailingAnchor).isActive = true
		
		// bottomConstraint is stored as a property so its constant can be adjusted based on the keyboard.
		
		self.bottomConstraint = self.view.bottomAnchor.constraint(equalTo: self.textView.bottomAnchor)
		
		self.bottomConstraint?.isActive = true
		
		let suggestionsViewHeight: CGFloat = 44.0
		
		// Tag Suggestions - Autolayout doesn't seem to work with inputAccessoryViews so frame based layout it is!
		
		self.tagsSuggestionView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: suggestionsViewHeight)
		
		self.tagsSuggestionView.delegate = self
		self.textView.inputAccessoryView = self.tagsSuggestionView
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.tagsSuggestionView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.bounds.size.width, height: self.tagsSuggestionView.frame.size.height)
		if self.shouldShowKeyboardOnViewWillAppear {
			
			self.textView.becomeFirstResponder()
		}
		
		self.shouldShowKeyboardOnViewWillAppear = false
		
		NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		NotificationCenter.default.removeObserver(self)
	}
	
	// MARK: UI Updating
	
	func updateTagsSuggestionView(matches: [String?]?) {
		
		self.tagsSuggestionView.update(newTags: matches)
	}

	// MARK: Actions
	
	func handleKeyboardNotification(notification: NSNotification) {
		
		if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
			
			self.bottomConstraint?.constant = keyboardSize.height
			
			UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: UIViewAnimationOptions(), animations: { () -> Void in
				
				self.view.layoutIfNeeded()
				}, completion: nil)
		}
	}
	
	// MARK: Memory Management
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}	
}

extension ViewController: UITextViewDelegate {
	
	func textViewDidChange(_ textView: UITextView) {
		
		// Create and store tags based on the use of # in the text.
		
		let tagsAndUrls = DataTypeParser.extractInternetVariables(textView.text)
		
		let tags = self.dataTypeParser.createTags(forTextPositions: tagsAndUrls.tags)
		
		self.tagsController.update(newTags: tags)
		
		// Apply formatting to highlight the URLS and hashtags
		
		self.dataTypeParser.formatTextViewForInternetVariables(textView)
		
		if let tag = textView.currentEditedHashTag() {
		
			// Update the similar tags based on the currently edited tag.
			let matches = self.tagsController.checkForSimilarTags(to: tag)
			
			self.updateTagsSuggestionView(matches: matches)
		}
		else {
			
			self.updateTagsSuggestionView(matches: nil)
		}
	}
}

extension ViewController: TagSuggestionsViewDelegate {
	
	func tagSuggestionsView(tagSuggestionsView: TagsSuggestionView, didSelectTag tag: String) {
		
		// At the moment this simply adds the text but in the future it'll replace the current edited word.
		
		self.textView.insertText(tag)
	}
}

